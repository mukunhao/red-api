<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RedController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getQuestion',[RedController::class,'getQuestion']);
Route::post('/judgeQuestion',[RedController::class,'judgeQuestion']);

Route::get('/splitRedPacket',[RedController::class,'splitRedPacket']);
//优惠券落库
Route::post('/getCoupon',[RedController::class,'getCoupon']);
//奖励详情
Route::get('/redPacketDetail',[RedController::class,'redPacketDetail']);
//微信授权回调
Route::get('/getCode',[RedController::class,'getCode']);
//微信授权回调
Route::get('/wxLogin',[RedController::class,'wxLogin']);
//发送短信验证码
Route::post('/getSmsCode',[RedController::class,'getSmsCode']);
//身份证号码验证
Route::post('/identityVerification',[RedController::class,'identityVerification']);
//分享签名
Route::post('/getShareSign',[RedController::class,'getShareSign']);
//加入机构
Route::post('/joinOrgan',[RedController::class,'joinOrgan']);
//未领取过期优惠券
Route::get('/recycleCoupon',[RedController::class,'recycleCoupon']);
//分发优惠券
Route::get('/sendCoupon',[RedController::class,'sendCoupon']);
//公益活动报名
Route::post('/joinActivity',[RedController::class,'joinActivity']);
//公益活动状态
Route::get('/getActivityStatus',[RedController::class,'getActivityStatus']);
//红包补偿
Route::get('/redCompensate',[RedController::class,'redCompensate']);
