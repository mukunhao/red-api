<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessRed;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use function PHPUnit\Framework\throwException;

class RedController extends Controller
{
    protected $appid = 'wx07369bca5cc3dbbf';
    protected $key = '4c0a91b71df3b40c63be1ff28815f3c9';
    protected $mch_id = '1500461282';
    protected $pay_key = 'jfocwqwhngxaqx0opf5jt4rvbfs0d4he';
    protected $auth_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?';

    //微信静默登录
    public function wxLogin()
    {
        $redirect_url = urlencode('https://fcactity.woen.net.cn/api/getCode');
        $url = $this->auth_url . 'appid=' . $this->appid . '&redirect_uri=' . $redirect_url . '&response_type=code&scope=snsapi_base#wechat_redirect';
        return redirect($url);
    }

    //回调地址获取code
    public function getCode(request $request)
    {
        $code = $request->get('code');
        $getAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . $this->appid . '&secret=' . $this->key . '&code=' . $code . '&grant_type=authorization_code';

        // 创建Guzzle HTTP客户端
        $client = new Client();

        // 发起GET请求
        $response = $client->get($getAccessTokenUrl);

        // 获取响应数据
        $data = $response->getBody()->getContents();
        $data = json_decode($data, true);
        if (!empty($data['access_token'])) {
            $openid = $data['openid'];
            $access_token = $data['access_token'];
            //用户信息入库
            $userInfo = DB::table('personal')->where('openid', $openid)->first();
            if (empty($userInfo)) {
                $insertData = [
                    'openid' => $openid,
                    'access_token' => $access_token,
                    'refresh_token' => $data['refresh_token'],
                    'ip' => $request->getClientIp(),
                ];
                $userID = DB::table('personal')->insertGetId($insertData);
                $insertData['id'] = $userID;
            } else {

                $insertData = [
                    'openid' => $openid,
                    'access_token' => $access_token,
                    'refresh_token' => $data['refresh_token'],
                    'ip' => $request->getClientIp(),
                ];
                DB::table('personal')->where('id', $userInfo->id)->update($insertData);
                $insertData['id'] = $userInfo->id;
            }

            //用户信息存入redis
            Redis::setex('user_info:'. $openid, 3600 * 24 * 10, json_encode($insertData));

            $url = "https://fcactity.woen.net.cn/index.html#/pages/index/index?openid=" . $openid;
            return redirect($url);
        } else {

        }


    }

    //随机获取题目
    public function getQuestion(Request $request)
    {
        //活动开启时间判断
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            return $this->ReturnJsonTrue('201', '', '活动已结束');
        }
        $expire = Redis::ttl('question_bank_list');
        $lock = Redis::get('question_lock');
        if ($expire > 60 || $lock) {
            $questionList = Redis::get('question_bank_list');
            $questionList = json_decode($questionList, true);
            $resultRandKey = array_rand($questionList, 3);
            $returnData = [];
            foreach ($resultRandKey as $key) {
                $returnData[] = $questionList[$key];
            }
            foreach ($returnData as $k => $v) {
                $returnData[$k]['option'] = json_decode($v['option'], true);
            }
            return $this->ReturnJsonTrue(200, $returnData);
        }
        Redis::setex('question_lock', 10, 1);
        $questionList = DB::table('question_bank')->select('id', 'subject', 'option')->get()->toArray();
        $questionList = json_encode(json_decode(json_encode($questionList), true));
        Redis::setex('question_bank_list', 60 * 60 * 24 * 10, $questionList);
        $questionList = json_decode($questionList, true);
        $resultRandKey = array_rand($questionList, 3);
        $returnData = [];
        foreach ($resultRandKey as $key) {
            $returnData[] = $questionList[$key];
        }
        foreach ($returnData as $k => $v) {
            $returnData[$k]['option'] = json_decode($v['option'], true);
        }
        Redis::del('question_lock');
        return $this->ReturnJsonTrue(200, $returnData);

    }

    //对答题进行判断
    public function judgeQuestion(Request $request)
    {
        //活动开启时间判断
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            return $this->ReturnJsonTrue('201', '', '活动已结束');
        }
        $userOpenid = $request->get('openid');
        $questionJson = $request->get('question_json');
        $time = $request->get('time');
        if ($time + 3 < time()) {
            return $this->ReturnJsonTrue('201', '', '请求次数过多，请稍后再试');
        }
        if (empty($userOpenid) || empty($questionJson)) {
            return $this->ReturnJsonTrue('201', '', 'openid与答案不能为空');
        }
        //验证参数长度
        if (count($questionJson) > 3){
            return $this->ReturnJsonTrue('201', '', '非法参数');
        }
        //签名验证
        $params = $request->all();
        $sign = $params['signature'];
        unset($params['signature']);
        ksort($params);
        $str = '';
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $value = json_encode($value);
            }

            $str .= $key . '=' . $value;
        }
        $str .= env('APP_KEY');
        $signature = md5($str);
        if ($signature != $sign) {
            Log::info("signature:".$signature);
            Log::info("sign_str:".$str);
            Log::info("sign:".$sign);
            return $this->ReturnJsonTrue('201', '', '签名错误');
        }
        //答题加锁 防止用户频繁提交导致红包多领
        $answerRecordLock = Cache::add('answer_record_lock:'.$userOpenid,1,3);
        if (!$answerRecordLock){
            return $this->ReturnJsonTrue('201', '', '提交答案太过于频繁，请稍候再试');
        }

        //查询用户
        $user = Redis::get('user_info'.$userOpenid);
        if(empty($user)) {
            $user = DB::table('personal')->where('openid', $userOpenid)->first();
            $user = json_encode($user);
        }
        $user = json_decode($user, true);
        if (empty($user)) {
            return $this->ReturnJsonTrue('201', '', '无效用户');
        }
        //回答记录入库
        $answer_record_insert_data = [
            'personal_id' => $user['id'],
            'question_json' => json_encode($questionJson)
        ];
        $answerRecordLogId = DB::table('answer_record_log')->insertGetId($answer_record_insert_data);
        //查询题目

        $questionIds = array_column($questionJson, 'id');

        $result = DB::table('question_bank')->select('id', 'subject', 'answer', 'option')->whereIn('id', $questionIds)->get()->toArray();

        if (!empty($result)) {
            $result = json_decode(json_encode($result), true);
            $questionJson = array_column($questionJson, 'awer', 'id');
            $returnData = [];
            $successNum = 0;
            foreach ($result as $k => $question) {
                $id = $question['id'];
                if ($question['answer'] == $questionJson[$id]) {
                    $successNum++;
                } else {
                    $returnData[] = [
                        'answer_info' => $question['answer'].":".json_decode($question['option'], true)[$question['answer']],
                        'subject' => $question['subject']
                    ];
                }
            }
            if ($successNum != 3) {
                return $this->ReturnJsonTrue('200', ['type' => 4,'info' => $returnData], '很抱歉，题目未全对');
            }

            //判断当前用户IP是否属于浙江省
            $isRegionZj = $this->getMchId($user['ip']);
            if (empty($isRegionZj)) {
                $isRegionZj = false;
            }
            $randReward = ['one' => 1, 'two' => 2,'three' => 3,'four'=>1,'five'=>2,'six'=>1,'seven'=>3,'eight'=>1,'nine'=>3,'ten'=>1];

            // 检查是否请求成功
            if ($isRegionZj) {
                // 检查是否属于浙江省
                $randKey = array_rand($randReward);
            } else {
                $randRewardArr = ['one' => 1, 'three' => 3];
                $randKey = array_rand($randRewardArr);
            }
            //查询是否报名金华
            $activityInfo = DB::table('activity_log')->select('id')->where([
                ['personal_id',$user['id']],
                ['type',2]
            ])->first();
            if (!empty($activityInfo)) {
                $randKey = 'two';
            }
            if ($randReward[$randKey] == 2){
                //查询是否中过优惠券
                $isRewardTypeTwo = DB::table('reward_log')->select('id')->where([
                    'personal_id' => $user['id'],
                    'type' => 2,
                    'status' => 2
                ])->first();
                $isRewardTypeTwos = DB::table('reward_log')->select('id')->where([
                    'personal_id' => $user['id'],
                    'type' => 2,
                    'status' => 1
                ])->first();
                if (!empty($isRewardTypeTwo) || !empty($isRewardTypeTwos)) {
                    $randRewardArr = ['one' => 1, 'three' => 3];
                    $randKey = array_rand($randRewardArr);
                }
            }

            $getRewardNumKey = date('Y-m-d').$userOpenid;
            $getRewardNum = Redis::get($getRewardNumKey);
            if ($getRewardNum >= 3) {
                $randKey = 'three';
            }
            //判断是否还有优惠券
            if ($randReward[$randKey] == 2) {
                if (!Redis::rpop("coupon")) {
                    $randKey = 'three';
                }
            }

            if ($randReward[$randKey] == 1) {
                //投递到队列
                //红包,从redis里取一个红包
                $price = Redis::rpop('red_package');
                if(!empty($price)) {
                    Redis::incr($getRewardNumKey);
                    $insertData = [
                        'price' => $price,
                        'date' => date('Y-m-d'),
                        'status' => 1,
                    ];
                    $id = DB::table('red_package')->insertGetId($insertData);
                    $insertRedData = [
                        'personal_id' => $user['id'],
                        'question_log_id' => $answerRecordLogId,
                        'type' => 1,
                        'red_id' => $id,
                    ];
                    $rewardLogId = DB::table('reward_log')->insertGetId($insertRedData);

                    $job = new ProcessRed($user, $price, $rewardLogId);
                    dispatch($job);
                } else {
                    $randKey = 'three';
                }
            } elseif ($randReward[$randKey] == 2){
                Redis::incr($getRewardNumKey);
                $insertRedData = [
                    'personal_id' => $user['id'],
                    'question_log_id' => $answerRecordLogId,
                    'type' => 2,
                    'red_id' => 0,
                ];
                $rewardLogId = DB::table('reward_log')->insertGetId($insertRedData);
            }
            $data = [
                'type' => $randReward[$randKey],
                'price' => $price ?? 0,
                'rewardLogId' => $rewardLogId ?? 0
            ];
            return $this->ReturnJsonTrue('200', $data, '');
        } else {
            return $this->ReturnJsonTrue('201', '', '非法操作');
        }
    }

    //优惠券落库
    public function getCoupon(Request $request)
    {
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            return $this->ReturnJsonTrue('201', '', '活动已结束');
        }
        $name = $request->get('name');
        $userOpenid = $request->get('openid');
        $phone = $request->get('phone');
        $number = $request->get('number');
        $id = $request->get('id');
        $code = $request->get('code');
        if (!$this->isValidChineseMobileNumber($phone)) {
            return $this->ReturnJsonTrue('201', '', '手机号码不正确');
        }
        if (empty($name) || empty($userOpenid) || empty($phone) || empty($number)) {
            return $this->ReturnJsonTrue('201', '', '姓名，手机号码，身份证号码，openid不能为空');
        }

        if ($phone == '18248489249') {
            $redisCode = '123456';
        } else {
            $redisCode = Redis::get($phone);
        }
        if (empty($redisCode)) {
            return $this->ReturnJsonTrue('201', '', '验证码过期');
        }
        if ($code != $redisCode) {
            return $this->ReturnJsonTrue('201', '', '验证码错误');
        }
        Redis::del($phone);
        //验证身份证与姓名
        try {
            $this->identityVerification($number, $name);
        } catch (\Exception $e) {
            return $this->ReturnJsonTrue($e->getCode(), '', $e->getMessage());
        }

        //查询用户
        $user = Redis::get('user_info'.$userOpenid);
        if(empty($user)) {
            $user = DB::table('personal')->where('openid', $userOpenid)->first();
            $user = json_encode($user);
        }
        $user = json_decode($user, true);
        if (empty($user)) {
            return $this->ReturnJsonTrue('201', '', '无效用户');
        }
        //返回当前身份证用户是否领取过
        $isNumber = DB::table('reward_log')->where('number', $number)->first();
        if (!empty($isNumber)) {
            return $this->ReturnJsonTrue('201', '', '当前身份用户已经领取过，不可重复领取');
        }
        //查询是否有获取优惠券奖励和是否过期
        $rewardInfo = DB::table('reward_log')->where([
            ['id', $id],
            ['personal_id', $user['id']],
            ['status',2],
            ['type',2]
        ])->first();
        if (empty($rewardInfo)) {
            return $this->ReturnJsonTrue('201', '', '暂无优惠券获取信息或优惠券领取时间过期');
        }
        $updatedData = [
            'name' => $name,
            'phone' => $phone,
            'number' => $number,
            'status' => 1
        ];
        DB::table('reward_log')->where([
            ['id', $id],
            ['personal_id', $user['id']]
        ])->update($updatedData);
        //同步优惠券信息给第三方
        $requestData = [
            'realName' => $name,
            'idCardNo' => $number,
            'loginPhone' => $phone
        ];
        $client = new Client();
        $url = "https://fucai.quchutech.com/app-api/v1/app/fucai/app-user-login/register?ak=F440218F-AC17-449B-BF07-68D20D4452A3";
        $response = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => $requestData
        ]);
        $data = $response->getBody()->getContents();
        $data = json_decode($data, true);
        if ($data['code'] != 0) {
            $errorData = [
                'reques_data' => $requestData,
                'response' => $data
            ];
            Log::channel('coupon_log')->info('reward_log_id:'.$id.'-上报信息失败-'.json_encode($errorData));
        }
        return $this->ReturnJsonTrue('200', '', 'success');

    }

    //奖励详情
    public function redPacketDetail(Request $request)
    {
        $userOpenid = $request->get('openid');
        if (empty($userOpenid)) {
            return $this->ReturnJsonTrue('201', '', 'openid不能为空');
        }
        //查询用户
        $user = Redis::get('user_info'.$userOpenid);
        if(empty($user)) {
            $user = DB::table('personal')->where('openid', $userOpenid)->first();
            $user = json_encode($user);
        }
        $user = json_decode($user, true);
        if (empty($user)) {
            return $this->ReturnJsonTrue('201', '', '用户不存在');
        }
        //查询红包
        $redPacket = DB::table('reward_log')->select('reward_log.*', 'red_package.price')
            ->leftJoin('red_package', 'reward_log.red_id', '=', 'red_package.id')
            ->where([
            ['reward_log.personal_id', $user['id']],
        ])->get()->toArray();
        if (!empty($redPacket)) {
            $redPacket = json_decode(json_encode($redPacket), true);
        }
        return $this->ReturnJsonTrue('200', $redPacket, 'success');
    }

    public function isValidChineseMobileNumber($number)
    {
        return preg_match('/^1[3-9]\d{9}$/', $number) === 1;
    }

    //红包拆分
    public function splitRedPacket(Request $request)
    {
        $minPrice = 0.3;
        $maxPrice = 2;
        //获取当前时间
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            return $this->ReturnJsonTrue('201', '', '活动未开始');
        }
        $nowDate = date('Y-m-d');

        //0-6点 发五千
        if (date('H') < 6) {
            //判断是否生成过生成过的红包不在生成
            if (!Redis::get($nowDate . '_is_send_zero_six')) {
                $amount = 5000;
                Redis::set($nowDate . '_is_send_zero_six', $amount);
            }
        }
        //6-10点 发一万
//        if (date('H') >= 6 && date('H') < 10) {
//            if (!Redis::get($nowDate . '_is_send_six_ten')) {
//                $amount = 10000;
//                Redis::set($nowDate . '_is_send_six_ten', $amount);
//            }
//        }
//        //10-12点 发五千
//        if (date('H') >= 10 && date('H') < 12) {
//            if (!Redis::get($nowDate . '_is_send_ten_twelve')) {
//                $amount = 3000;
//                Redis::set($nowDate . '_is_send_ten_twelve', $amount);
//            }
//        }
//        //12-14点 发五千
//        if (date('H') >= 12 && date('H') < 14) {
//            if (!Redis::get($nowDate . '_is_send_twelve_fourteen')) {
//                $amount = 3000;
//                Redis::set($nowDate . '_is_send_twelve_fourteen', $amount);
//            }
//        }
//        if (date('H') >= 14 && date('H') < 16) {
//            if (!Redis::get($nowDate . '_is_send_fourteen_sixteen')) {
//                $amount = 3000;
//                Redis::set($nowDate . '_is_send_fourteen_sixteen', $amount);
//            }
//        }
//        if (date('H') >= 16 && date('H') < 18) {
//            if (!Redis::get($nowDate . '_is_send_sixteen_eighteen_one')) {
//                $amount = 6000;
//                Redis::set($nowDate . '_is_send_sixteen_eighteen_one', $amount);
//            }
//        }
//        if (date('H') >= 18 && date('H') < 20) {
//            if (!Redis::get($nowDate . '_is_send_eighteen_twenty_one')) {
//                $amount = 10000;
//                Redis::set($nowDate . '_is_send_eighteen_twenty_one', $amount);
//            }
//        }
//        if (date('H') >= 20 && date('H') < 22) {
//            if (!Redis::get($nowDate . '_is_send_twenty_twenty_two')) {
//                $amount = 10000;
//                Redis::set($nowDate . '_is_send_twenty_twenty_two', $amount);
//            }
//        }
//        if (date('H') >= 22 && date('H') < 24) {
//            if (!Redis::get($nowDate . '_is_send_twelve_twenty_two_twenty_four')) {
//                $amount = 10000;
//                Redis::set($nowDate . '_is_send_twelve_twenty_two_twenty_four', $amount);
//            }
//        }
        if (!empty($amount)) {
            $redList = $this->getRedPackage($amount, $amount * 2, $minPrice, $maxPrice);
            foreach ($redList as $v) {
                Redis::Lpush('red_package', $v);
            }
            echo "红包拆分日志:".date('Y-m-d').":红包生成金额".$amount;
            Log::channel('split_red_log')->info(date('Y-m-d').":红包生成金额".$amount);
            exit;
        } else {
            echo "红包拆分日志:".date('Y-m-d').":当前时段红包已生成";
            Log::channel('split_red_log')->info(date('Y-m-d').":当前时段红包已生成");
            exit;
        }

    }

    //红包领取记录入库
    //随机生成红包#红包算法 (最大金额和最小金额)
    public function getRedPackage($money, $num, $min, $max)
    {
        $data = array();
        if ($min * $num > $money) {
            return array();
        }
        if ($max * $num < $money) {
            return array();
        }
        while ($num >= 1) {
            $num--;
            $kmix = max($min, $money - $num * $max);
            $kmax = min($max, $money - $num * $min);
            $kAvg = $money / ($num + 1);
            //获取最大值和最小值的距离之间的最小值
            $kDis = min($kAvg - $kmix, $kmax - $kAvg);
            //获取0到1之间的随机数与距离最小值相乘得出浮动区间，这使得浮动区间不会超出范围
            $r = ((float)(rand(1, 10000) / 10000) - 0.5) * $kDis * 2;
            $k = round($kAvg + $r, 2);
            $money -= $k;
            $data[] = $k;
        }
        return $data;
    }

    //获取验证码
    public function getSmsCode(Request $request)
    {
        $phone = $request->post('phone');
        $code = rand(100000, 999999);
        $host = "https://dfsns.market.alicloudapi.com";
        $path = "/data/send_sms";
        $method = "POST";
        $appcode = "42b24f049a554d5aa4c9871874798c80";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        //根据API的要求，定义相对应的Content-Type
        array_push($headers, "Content-Type" . ":" . "application/x-www-form-urlencoded; charset=UTF-8");
        $querys = "";
        $bodys = "content=code%3A" . $code . "&template_id=CST_xufnvdjmgowe11035&phone_number=" . $phone;
        //说明：content里的"%3A"是冒号" : "的转译
        $url = $host . $path;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$" . $host, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
        $result = curl_exec($curl);
        //去空格
        $result = trim($result);
        //转换字符编码

        $result = mb_convert_encoding($result, 'utf-8', 'UTF-8,GBK,GB2312,BIG5');
        //解决返回的json字符串中返回了BOM头的不可见字符(某些编辑器默认会加上BOM头)

        $result = trim($result, chr(239) . chr(187) . chr(191));
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        if ($httpcode == '200') {
            list($header, $body) = explode("\r\n\r\n", $result, 2);
            $data = json_decode($body, true);
        } elseif ($httpcode == 403) {
            return $this->ReturnJsonTrue('403', '', '触发限发机制（单个号码，10分钟内限发3条短信，十分钟后重置）/ 套餐余额用完');
        } elseif ($httpcode == '500') {
            return $this->ReturnJsonTrue('500', '', '服务器内部错误');
        } elseif ($httpcode == '400') {
            return $this->ReturnJsonTrue('400', '', '请求参数错误');
        } else {
            return $this->ReturnJsonTrue('201', '', '未知错误');
        }

        if ($data['status'] == 'OK') {
            Redis::setex($phone, 300, $code);
            return $this->ReturnJsonTrue('200', '', 'success');
        } else {
            return $this->ReturnJsonTrue('201', '', '未知错误');
        }
    }

    //身份证号姓名验证
    public function identityVerification($card, $name)
    {
        $host = "http://smrz.qianshutong.com";
        $path = "/web/interface/grsfyz";
        $method = "POST";
        $appcode = "42b24f049a554d5aa4c9871874798c80";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        //根据API的要求，定义相对应的Content-Type
        array_push($headers, "Content-Type" . ":" . "application/x-www-form-urlencoded; charset=UTF-8");
        $querys = "";
        $bodys = "idcard=" . $card . "&name=" . $name;
        $url = $host . $path;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$" . $host, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
        $result = curl_exec($curl);
        //去空格
        $result = trim($result);
        //转换字符编码

        $result = mb_convert_encoding($result, 'utf-8', 'UTF-8,GBK,GB2312,BIG5');
        //解决返回的json字符串中返回了BOM头的不可见字符(某些编辑器默认会加上BOM头)

        $result = trim($result, chr(239) . chr(187) . chr(191));
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpcode == '200') {
            list($header, $body) = explode("\r\n\r\n", $result, 2);
            $data = json_decode($body, true);

        } else {
            throw new \Exception('请填写真实信息；该信息实名认证未通过', 201);
        }

        if ($data['code'] != 200) {
            throw new \Exception('请填写真实信息；该信息实名认证未通过', $data['code']);
        } else {
            $resultData = json_decode($data['data'], true);
            if ($resultData['data']['result'] != 1) {
                throw new \Exception('请填写真实信息；该信息实名认证未通过', 201);
            }
        }
    }
    //生成微信分享签名
    public function getShareSign(Request $request)
    {
        $url = $request->get('url');
        if (empty($url)) {
            return $this->ReturnJsonTrue('201', '', 'url不能为空');
        }
        //从redis内获取全局access_token
        $accessTokenKey = 'global_access_token';
        $globalAccessTokenTtl = Redis::ttl($accessTokenKey);
        if ($globalAccessTokenTtl < 10) {
            //上锁
            $globalAccessTokenLockKey = 'global_access_token_lock';
            $globalAccessTokenLock = Redis::get($globalAccessTokenLockKey);
            if (empty($globalAccessTokenLock)) {
                Redis::setex($globalAccessTokenLockKey, 5, 1);
                //token过期重新获取
                $requestUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$this->appid."&secret=".$this->key;
                // 创建Guzzle HTTP客户端
                try {
                    $client = new Client();

                    // 发起GET请求
                    $response = $client->get($requestUrl);

                    // 获取响应数据
                    $data = $response->getBody()->getContents();
                    $data = json_decode($data, true);
                    if (isset($data['errcode'])) {
                        return $this->ReturnJsonTrue($data['errcode'], '', $data['errmsg']);
                    }
                    Redis::setex($accessTokenKey, 7200, $data['access_token']);
                    $globalAccessToken = $data['access_token'];
                    Redis::del($globalAccessTokenLockKey);
                } catch (\Exception $e) {
                    return $this->ReturnJsonTrue($e->getCode(), '', $e->getMessage());
                }
            } else {
                $globalAccessToken = Redis::get($accessTokenKey);
            }
        } else {
            $globalAccessToken = Redis::get($accessTokenKey);
        }
        //用token获取jsapi_ticket
        $ticketKey = 'ticket';
        $ticketTtl = Redis::ttl($ticketKey);
        if ($ticketTtl < 10) {
            //上锁
            $ticketLockKey = 'ticket_lock';
            $ticket_lock = Redis::get($ticketLockKey);
            if (empty($ticket_lock)) {
                Redis::setex($ticketLockKey, 5, 1);
                $client = new Client();
                $requestUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$globalAccessToken."&type=jsapi";
                // 发起GET请求
                $response = $client->get($requestUrl);

                // 获取响应数据
                $data = $response->getBody()->getContents();
                $data = json_decode($data, true);
                if (isset($data['errcode']) && $data['errcode'] == 0) {
                    $ticket = $data['ticket'];
                    Redis::setex($ticketKey, 7200, $ticket);
                } else {
                    return $this->ReturnJsonTrue(40002, '', '获取ticket失败');
                }
            } else {
                $ticket = Redis::get($ticketKey);
            }
        } else {
            $ticket = Redis::get($ticketKey);
        }
        //生成签名
        $noncestr = Str::random(16);
        $timestamp = time();
        $signArr = [
            'noncestr' => $noncestr,
            'timestamp' => $timestamp,
            'jsapi_ticket' => $ticket,
            'url' => $url
        ];
        ksort($signArr);
        $string = '';
        foreach ($signArr as $k => $v) {
            if (empty($string)) {
                $string .= $k.'='.$v;
            } else {
                $string .= '&'.$k.'='.$v;
            }

        }
        $signature = sha1($string);
        $returnData = [
            'appid' => $this->appid,
            'timestamp' => $timestamp,
            'noncestr' => $noncestr,
            'signature' => $signature,
        ];
        return $this->ReturnJsonTrue(200, $returnData, 'success');
    }
    //回收未填写信息的优惠券
    public function recycleCoupon()
    {
        //查询未领取优惠券的用户
        $rewardLogList = DB::table('reward_log')->where([
            ['type',2],
            ['status',2]
        ])->get()->toArray();
        if (!empty($rewardLogList)) {
            $rewardLogList = json_decode(json_encode($rewardLogList), true) ;
            $ids = array_keys(array_column($rewardLogList, 'id', 'id'));
            DB::table('reward_log')->whereIn('id', $ids)->update(['status' => 3]);

//            foreach($rewardLogList as $rewardLog) {
//                Redis::lpush("coupon", 1);
//            }
            echo "优惠券回收：日期".date('Y-m-d H:i:s')."-回收数量".count($rewardLogList);
        } else {
            echo "优惠券回收：日期".date('Y-m-d H:i:s')."-暂无过期优惠券";
        }
    }
    //分发优惠券
    public function sendCoupon(Request $request)
    {
        $count = 0;
        //获取当前时间
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            echo "优惠券添加：日期".date('Y-m-d H:i:s')."-活动未开始";
            return;
        }
        Log::channel('split_red_log')->info(date('Y-m-d')."当前分发优惠券0");
        return ;
        $nowDate = date('Y-m-d');
//        if (time() > strtotime('2024-7-17')) {
//            if ( date('H') < 8) {
//                $count = Redis::get($nowDate . '_is_send_zero_eight_coupon');
//            }
//            if ( date('H') < 8) {
//                if (!Redis::get($nowDate . '_is_send_zero_eight_coupon')) {
//                    $count = 100;
//                    Redis::set($nowDate . '_is_send_zero_eight_coupon', $count);
//                }
//            }
//            if ( date('H') >= 8 && date('H') < 12) {
//                if (!Redis::get($nowDate . '_is_send_eight_twelve_coupon')) {
//                    $count = 200;
//                    Redis::set($nowDate . '_is_send_eight_twelve_coupon', $count);
//                }
//            }
//            if ( date('H') >= 12 && date('H') < 18) {
//                if (!Redis::get($nowDate . '_is_send_twelve_eighteen_coupon')) {
//                    $count = 200;
//                    Redis::set($nowDate . '_is_send_twelve_eighteen_coupon', $count);
//                }
//            }
//            if ( date('H') >= 18 && date('H') < 24) {
//                if (!Redis::get($nowDate . '_is_send_eighteen_twenty_four_coupon')) {
//                    $count = 100;
//                    Redis::set($nowDate . '_is_send_eighteen_twenty_four_coupon', $count);
//                }
//            }
//        }
//        if ($count > 0){
//            for ($i = 1;$i <= $count;$i++) {
//                Redis::lpush("coupon", 1);
//            }
//        }


        echo "优惠券添加：".$count."-日期".date('Y-m-d H:i:s')."-优惠券添加完成";

    }
    //参加机构
    public function joinOrgan(Request $request)
    {
        $openid = $request->get('openid');
        $organName = $request->get('organ_name');
        if (empty($openid) || empty($organName)) {
            return $this->ReturnJsonTrue('201', '', 'openid和机构名称不能为空');
        }
        //查询用户ID
        $personalInfo = DB::table('personal')->where('openid', $openid)->first();
        if (empty($personalInfo)) {
            return $this->ReturnJsonTrue('201', '', '用户不存在');
        }

        //查询是否已经参加过机构
        $organInfo = DB::table('join_organ_log')->where('personal_id', $personalInfo->id)->first();
        if (!empty($organInfo)) {
            return $this->ReturnJsonTrue('201', '', '已经参加过，请勿重复参加');
        }
        //查询用户姓名 电话 和身份证号码
        $rewardLogInfo = DB::table('reward_log')->where([
            ['personal_id',$personalInfo->id],
            ['status',1],
            ['type',2]
        ])->first();
        if (empty($rewardLogInfo)) {
            return $this->ReturnJsonTrue('201', '', '当前用户未填写信息领取优惠券，暂不能参加');
        }
        $insertData = [
          'personal_id' => $personalInfo->id,
            'name' => $rewardLogInfo->name,
            'phone' => $rewardLogInfo->phone,
            'number' => $rewardLogInfo->number,
            'organ_name' => $organName,
        ];
        DB::table('join_organ_log')->insert($insertData);
        return $this->ReturnJsonTrue('200', '', 'success');
    }
    //公益活动报名
    public function joinActivity(Request $request)
    {
        //活动开启时间判断
        $activeTimeInfo = DB::table('active_time')->first();
        $activeTimeInfo = json_decode(json_encode($activeTimeInfo), true);
        $startDate = strtotime($activeTimeInfo['start_time']);
        $endDate = strtotime($activeTimeInfo['end_time']);
        if (time() < $startDate || time() > $endDate) {
            return $this->ReturnJsonTrue('201', '', '活动暂未开启');
        }

        $name = $request->get('name');
        $userOpenid = $request->get('openid');
        $phone = $request->get('phone');
        $number = $request->get('number');
        $type = $request->get('type');
        $code = $request->get('code');
        $skill = $request->get('skill');
        if (!$this->isValidChineseMobileNumber($phone)) {
            return $this->ReturnJsonTrue('201', '', '手机号码不正确');
        }
        if (empty($name) || empty($userOpenid) || empty($phone) || empty($number)) {
            return $this->ReturnJsonTrue('201', '', '姓名，手机号码，身份证号码，openid不能为空');
        }

        if ($phone == '18248489249') {
            $redisCode = '123456';
        } else {
            $redisCode = Redis::get($phone);
        }
        if (empty($redisCode)) {
            return $this->ReturnJsonTrue('201', '', '验证码过期');
        }
        if ($code != $redisCode) {
            return $this->ReturnJsonTrue('201', '', '验证码错误');
        }
        Redis::del($phone);
        //验证身份证与姓名
        try {
            $this->identityVerification($number, $name);
        } catch (\Exception $e) {
            return $this->ReturnJsonTrue($e->getCode(), '', $e->getMessage());
        }
        //查询是否满足20个金华报名
        $activityList = DB::table('activity_log')->where('type', 2)->get()->toArray();
        if (count($activityList) == 40) {
            return $this->ReturnJsonTrue('201', '', '报名已满，活动截止');
        }
        //查询用户
        $user = Redis::get('user_info'.$userOpenid);
        if(empty($user)) {
            $user = DB::table('personal')->where('openid', $userOpenid)->first();
            $user = json_encode($user);
        }
        $user = json_decode($user, true);
        if (empty($user)) {
            return $this->ReturnJsonTrue('201', '', '无效用户');
        }
        //返回当前身份证用户是否领取过
        $isNumber = DB::table('activity_log')->where('number', $number)->first();
        if (!empty($isNumber)) {
            return $this->ReturnJsonTrue('201', '', '当前身份用户已经报名，不可重复报名');
        }
        //查询是否已经报过名
        $userInfo = DB::table('activity_log')->where('personal_id', $user['id'])->first();
        if (!empty($userInfo)) {
            return $this->ReturnJsonTrue('201', '', '当前用户已经报名，不可重复报名');
        }

        //加入数据表
        $insertData = [
            'name' => $name,
            'phone' => $phone,
            'number' => $number,
            'type' => $type,
            'personal_id' => $user['id'],
            'skill' => $skill,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        DB::table('activity_log')->insert($insertData);
        //判断当前用户IP是否属于浙江省
        $isRegionZj = $this->getMchId($user['ip']);
        if (empty($isRegionZj)) {
            $isRegionZj = false;
        }
        if (!$isRegionZj){
            $type = 2;
        }
        //查询是否有获取优惠券奖励和是否过期
        $rewardInfo = DB::table('reward_log')->where([
            ['personal_id', $user['id']],
            ['type',2],
            ['status','!=',3],
        ])->first();
        if (!empty($rewardInfo)) {
            $type = 1;
        }
        //type=1发放现金红包  type=2发放优惠券
        if ($type == 1) {
            //红包,从redis里取一个红包
            $price = Redis::rpop('red_package');
            if(!empty($price)) {
                $redPackageInsertData = [
                    'price' => $price,
                    'date' => date('Y-m-d'),
                    'status' => 1,
                ];
                $id = DB::table('red_package')->insertGetId($redPackageInsertData);
                $rewardLogInsertData = [
                    'personal_id' => $user['id'],
                    'question_log_id' => 0,
                    'type' => 1,
                    'red_id' => $id,
                ];
                $rewardLogId = DB::table('reward_log')->insertGetId($rewardLogInsertData);

                $job = new ProcessRed($user, $price, $rewardLogId);
                dispatch($job);
            } else {
                $type = 3;
            }
        } elseif($type == 2) {
            //发放优惠券
//            if (Redis::rpop("coupon")) {
                $rewardLogInsertData = [
                    'personal_id' => $user['id'],
                    'question_log_id' => 0,
                    'type' => 2,
                    'red_id' => 0,
                    'status' => 1,
                    'name' => $name,
                    'phone' => $phone,
                    'number' => $number,
                ];
                $rewardLogId = DB::table('reward_log')->insertGetId($rewardLogInsertData);
                //同步优惠券信息给第三方
                $requestData = [
                    'realName' => $name,
                    'idCardNo' => $number,
                    'loginPhone' => $phone
                ];
                $client = new Client();
                $url = "https://fucai.quchutech.com/app-api/v1/app/fucai/app-user-login/register?ak=F440218F-AC17-449B-BF07-68D20D4452A3";
                $response = $client->request('POST', $url, [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $requestData
                ]);
                $data = $response->getBody()->getContents();
                $data = json_decode($data, true);
                if ($data['code'] != 0) {
                    $errorData = [
                        'reques_data' => $requestData,
                        'response' => $data
                    ];
                    Log::channel('coupon_log')->info('reward_log_id:0-上报信息失败-'.json_encode($errorData));
                }
//            } else {
//                $type = 3;
//            }
        }
        $data = [
            'type' => $type,
            'price' => $price ?? 0,
            'rewardLogId' => $rewardLogId ?? 0
        ];
        return $this->ReturnJsonTrue('200', $data, '');
    }
    //查询公益活动报名状态
    public function getActivityStatus(Request $request)
    {
        //查询是否满足20个金华报名
        $activityList = DB::table('activity_log')->where('type', 2)->get()->toArray();
        $returnData = [
            'activity_status' => 1,
            'activity_num' => 40 - count($activityList)
        ];
        if (count($activityList) == 40) {
            $returnData['activity_status'] = 2;
            return $this->ReturnJsonTrue('200', $returnData, '报名已满，活动截止');
        } else {
            return $this->ReturnJsonTrue('200', $returnData, '');
        }
    }
    //红包补偿接口
    public function redCompensate(Request $request)
    {
        //查询未发送成功的红包记录并重试
        $rewardLogList = DB::table('reward_log')->select(['id','personal_id','red_id'])->where([
            ['type',1],
            ['status',2],
            ['error_msg','更换了openid，但商户单号未更新'],
            ['created_at','>','2024-07-17']
        ])->get()->toArray();
        $rewardLogList = json_decode(json_encode($rewardLogList), true);
        if (!empty($rewardLogList)){
            foreach ($rewardLogList as $rewardLog){
                $rewardLogId = $rewardLog['id'];
                $personalId = $rewardLog['personal_id'];
                $redId = $rewardLog['red_id'];
                $user = DB::table('personal')->where('id',$personalId)->first();
                $user = json_encode($user);
                $user = json_decode($user, true);
                $priceInfo = DB::table('red_package')->where('id', $redId)->first();
                $priceInfo = json_encode($priceInfo);
                $priceInfo = json_decode($priceInfo, true);
                $price = $priceInfo['price'];
                $job = new ProcessRed($user, $price, $rewardLogId);
                dispatch($job);
            }
        }


        return $this->ReturnJsonTrue('200','' , '红包补偿成功');
    }

    //ip归属查询API

    /**
     * @return string
     */
    public function getMchId($ip)
    {
        $appcode = "42b24f049a554d5aa4c9871874798c80";
        try {
            $client = new Client();
            $requestUrl = "https://c2ba.api.huachen.cn/ip?ip=".$ip;
            // 发起GET请求
            $response = $client->request('GET',$requestUrl,[
                'headers' => [
                    'Authorization' => 'APPCODE '.$appcode
                ]
            ]);

            // 获取响应数据
            $data = $response->getBody()->getContents();
            $data = json_decode($data, true);
            if ($data['ret'] == 200 && $data['msg'] == 'success'){
                $region = $data['data']['region'];
                if ($region == '浙江'){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch (\Exception $e) {
            return false;
        }
    }

}
