<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function ReturnJsonTrue($code = 200, $data = [], $msg = '')
    {
        return response()->json(['code' => $code, 'data' => $data,'msg' => $msg]);
    }

    public function ReturnJsonFalse($code = 500, $msg = '')
    {
        return response()->json(['code' => $code, 'msg' => $msg]);
    }
}
