<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class ProcessRed implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $user;
    protected $rewardLogId;
    protected $price;
    protected $mch_id = '1500461282';
    protected $pay_key = 'jfocwqwhngxaqx0opf5jt4rvbfs0d4he';
    protected $appid = 'wx07369bca5cc3dbbf';
    protected $key = '4c0a91b71df3b40c63be1ff28815f3c9';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $price, $rewardLogId)
    {
        $this->user = $user;
        $this->price = $price;
        $this->rewardLogId = $rewardLogId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->transfer($this->user, $this->price, $this->rewardLogId);
    }
    //商家转账
    public function transfer($user, $price, $rewardLogId)
    {
        //查询是否有相同订单号
        do{
            $billno =  $this->mch_id.date('md').round(microtime(true) * 1000);
            $billnoLock = Cache::add('billno_lock:'.$billno,1,3);

        }while(!$billnoLock);
        //查询当前红包是否被发放
        $rewardLogInfo = DB::table('reward_log')->select('id')->where([
            ['id',$rewardLogId],
            ['status',1]
        ])->first();
        if (!empty($rewardLogInfo)) {
            $logData = [
              'user' => $user,
              'price' => $price,
              'reward_log_id' => $rewardLogId,
              'date' => date('Y-m-d'),
            ];
            Log::channel('transfer')->info("当前红包已被领取：红包信息".json_encode($logData));
            return;
        }
        $url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
        $obj['nonce_str'] = md5(rand(1, 100));
        $obj['mch_billno'] = $billno;
        $obj['mch_id'] = $this->mch_id;
        $obj['wxappid'] = $this->appid;
        $obj['send_name'] = '浙江福彩中心';
        $obj['re_openid'] = $user['openid'];
        $obj['total_amount'] = $price * 100;
        $obj['total_num'] = 1;
        $obj['wishing'] = '感谢您参与浙江福彩公益问答活动，祝您暑期愉快';
        $obj['client_ip'] = '47.96.17.153';
        $obj['act_name'] = '浙江福彩邀您公益问答';
        $obj['remark'] = '邀请好友一块参加吧';
        $obj['scene_id'] = 'PRODUCT_2';
        $sign = $this->get_sign($obj, false);
        $obj['sign'] = $sign;
        $sendData = $this->arr_to_xml($obj);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "{$url}");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $sendData);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($curl, CURLOPT_SSLCERT, app_path('Utils/apiclient_cert.pem'));
        curl_setopt($curl, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($curl, CURLOPT_SSLKEY, app_path('Utils/apiclient_key.pem'));
        $string = curl_exec($curl);
        curl_close($curl);
        Log::info($string);
        // 使用simplexml_load_string函数解析XML字符串
        $strpostData = simplexml_load_string($string, 'SimpleXMLElement', LIBXML_NOCDATA);

        Log::info('redpackageinfo:'.json_encode($strpostData));
        $strpostData = json_decode(json_encode($strpostData), true);
        if($strpostData["return_code"] == "SUCCESS" && $strpostData["result_code"] == "SUCCESS") {
            // 发红包成功...修改领取日志状态
            $updatedData = [
              'status' => 1,
              'error' => json_encode($strpostData),
              'billno' => $obj['mch_billno']
            ];
            DB::table('reward_log')->where([
                ['id',$rewardLogId],
                ['personal_id',$user['id']]
            ])->update($updatedData);

        } else {
            $logData = [
                'user' => $user,
                'price' => $price,
                'reward_log_id' => $rewardLogId,
                'date' => date('Y-m-d'),
                'error_msg' => $strpostData
            ];
            $updatedData = [
                'error' => json_encode($strpostData),
                'error_msg' => $strpostData['return_msg'],
                'billno' => $obj['mch_billno']
            ];
            DB::table('reward_log')->where([
                ['id',$rewardLogId],
                ['personal_id',$user['id']]
            ])->update($updatedData);
            Log::channel('transfer_error')->info("当前红包发放失败:".json_encode($logData));
        }


    }
    public function arr_to_xml($data, $root = true)
    {
        $str = "";
        if ($root) {
            $str .= "<xml>";
        }
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $child = arr_to_xml($val, false);
                $str .= "<$key>$child</$key>";
            } else {
                $str .= "<$key><![CDATA[$val]]></$key>";
            }
        }
        if ($root) {
            $str .= "</xml>";
        }
        return $str;
    }
    /**
     * @param $arr 生成前面的参数
     * @param $urlencode
     * @return string 返回加密后的签名
     */
    public function get_sign($arr, $urlencode)
    {
        $buff = "";
        //对传进来的数组参数里面的内容按照字母顺序排序，a在前面，z在最后（字典序）
        ksort($arr);
        foreach ($arr as $k => $v) {
            if (null != $v && "null" != $v && "sign" != $k) {    //签名不要转码
                if ($urlencode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        // 去掉末尾符号“&”，其实不用这个if，因为长度肯定大于0
        if (strlen($buff) > 0) {
            $stringA = substr($buff, 0, strlen($buff) - 1);
        }
        //签名拼接api
        $stringSignTemp = $stringA . "&key=" . $this->pay_key;
        //签名加密并大写
        $sign = strtoupper(md5($stringSignTemp));
        return $sign;
    }
}
